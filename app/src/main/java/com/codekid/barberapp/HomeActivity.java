package com.codekid.barberapp;

import android.app.AlertDialog;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codekid.barberapp.Common.Common;

import com.codekid.barberapp.Fragments.HomeFragment;
import com.codekid.barberapp.Fragments.ShopFragment;
import com.codekid.barberapp.Models.User;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    BottomSheetDialog bottomSheetDialog;

    CollectionReference userRef;

    AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        ButterKnife.bind(HomeActivity.this);



        //Init
        userRef = FirebaseFirestore.getInstance().collection("Users");
        alertDialog = new SpotsDialog.Builder().setContext(this).setCancelable(false).build();

        //Check intent , if login == true ,enable full access
        //if login == false, user can only look around app
        if(getIntent() != null)
        {
            boolean isLogin = getIntent().getBooleanExtra(Common.IS_LOGIN,false);
            if (isLogin)
            {
                alertDialog.show();
                //check if user exists
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(final Account account) {
                        if(account != null)
                        {
                            //save userPhone by Paper
                            Paper.init(HomeActivity.this);
                            Paper.book().write(Common.LOGGED_KEY,account.getPhoneNumber());

                            DocumentReference currentUer = userRef.document(account.getPhoneNumber().toString());
                            currentUer.get()
                                    .addOnCompleteListener(task -> {
                                        if(task.isSuccessful())
                                        {
                                            DocumentSnapshot userSnap = task.getResult();
                                            if(!userSnap.exists())
                                            {

                                                showUpdateDialog(account.getPhoneNumber().toString());
                                            }
                                            else
                                            {
                                                //if user is already in the system
                                                Common.currentUser = userSnap.toObject(User.class);
                                                bottomNavigationView.setSelectedItemId(R.id.action_home);
                                            }
                                            if(alertDialog.isShowing())
                                                alertDialog.dismiss();

                                        }

                                    });
                        }
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                        Toast.makeText(HomeActivity.this, ""+accountKitError.getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

        //View
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            Fragment fragment = null;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.action_home)
                    fragment = new HomeFragment();
                else if(item.getItemId() == R.id.action_shop)
                    fragment = new ShopFragment();
                return loadFragment(fragment);
            }
        });


    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
            return  true;
        }
        return  false;
    }

    private void showUpdateDialog(final String phoneNumber) {
        //Init dialog
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setTitle("One More Step");
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        View sheetView = getLayoutInflater().inflate(R.layout.update_info_layout,null);

        Button btn_update = sheetView.findViewById(R.id.btn_update);
        final TextInputEditText edt_name = sheetView.findViewById(R.id.edt_name);
        final TextInputEditText edt_address = sheetView.findViewById(R.id.edt_address);

        btn_update.setOnClickListener(v -> {
            if(!alertDialog.isShowing())
                alertDialog.show();

            final User  user = new User(edt_name.getText().toString(),edt_address.getText().toString(),phoneNumber);
            userRef.document(phoneNumber)
                    .set(user)
                    .addOnSuccessListener(aVoid -> {
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();

                        Common.currentUser = user;
                        bottomNavigationView.setSelectedItemId(R.id.action_home);

                        bottomSheetDialog.dismiss();
                        Toast.makeText(HomeActivity.this, "Thank You", Toast.LENGTH_SHORT).show();
                    }).addOnFailureListener(e -> {
                        bottomSheetDialog.dismiss();
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();
                        Toast.makeText(HomeActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    });
        });

        bottomSheetDialog.setContentView(sheetView);
        bottomSheetDialog.show();
    }
}
