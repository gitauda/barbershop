package com.codekid.barberapp.Interface;

import java.util.List;

public interface IBarberShopLoadListener {
    void onBarberShopLoadSuccess(List<String> areaNameList);
    void onBarberShopLoadFailed(String message);
}
