package com.codekid.barberapp.Interface;

import android.view.View;

public interface IItemSelectedListener {
    void onItemSelectedListener(View view,int position);
}
