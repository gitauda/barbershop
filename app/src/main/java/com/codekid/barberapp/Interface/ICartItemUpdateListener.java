package com.codekid.barberapp.Interface;

public interface ICartItemUpdateListener {
    void onCartItemUpdateSuccess();
}
