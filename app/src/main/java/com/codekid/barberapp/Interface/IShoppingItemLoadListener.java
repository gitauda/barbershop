package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.ShoppingItem;

import java.util.List;

public interface IShoppingItemLoadListener {
    void onShoppingItemLoadSuccess(List<ShoppingItem> shoppingItems);
    void onShoppingItemLoadFailure(String message);
}
