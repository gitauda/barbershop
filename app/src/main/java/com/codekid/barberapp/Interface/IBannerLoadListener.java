package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.Banner;

import java.util.List;

public interface IBannerLoadListener {
    void onBannerLoadSuccess(List<Banner> banners);
    void onBannerLoadFailed(String message);
}
