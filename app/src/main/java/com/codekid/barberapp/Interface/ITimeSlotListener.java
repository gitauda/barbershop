package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.TimeSlot;

import java.util.List;

public interface ITimeSlotListener {
    void onTimeSlotLoadSuccess(List<TimeSlot> timeSlots);
    void onTimeSlotLoadFailure(String message);
    void onTimeSlotLoadEmpty();
}
