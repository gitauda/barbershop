package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.BarberShops;

import java.util.List;

public interface IBranchLoadListener {
    void onBranchLoadSuccess(List<BarberShops> shopsList );
    void onBranchLoadFailure(String message);
}
