package com.codekid.barberapp.Interface;

public interface ICartItemCountListener {
    void onCartItemCountSuccess(int count);
}
