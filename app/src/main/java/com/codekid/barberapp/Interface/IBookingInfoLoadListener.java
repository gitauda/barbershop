package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.BookingInfo;

public interface IBookingInfoLoadListener {
    void onBookingInfoLoadEmpty();
    void onBookingInfoLoadSuccess(BookingInfo bookingInfo,String documentId);
    void onBookingInfoLoadFailed(String message);
}
