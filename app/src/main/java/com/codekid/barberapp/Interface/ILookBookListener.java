package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Models.Banner;

import java.util.List;

public interface ILookBookListener {
    void onLookBookLoadSuccess(List<Banner> banners);
    void onLookBookLoadFailed(String message);
}
