package com.codekid.barberapp.Interface;

import com.codekid.barberapp.Database.CartItem;

import java.util.List;

public interface IcartItemLoadListener {
    void onGetAllItemsFromCartSuccess(List<CartItem> cartItems);
}
