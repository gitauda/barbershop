package com.codekid.barberapp.Interface;

public interface IBookingInfoChangeListener {
    void onBookingInfoChanged();
}
