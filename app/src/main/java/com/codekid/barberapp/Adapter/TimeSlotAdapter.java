package com.codekid.barberapp.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Interface.IItemSelectedListener;
import com.codekid.barberapp.Models.TimeSlot;
import com.codekid.barberapp.R;

import java.util.ArrayList;
import java.util.List;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.TimeSlotViewHolder> {

    Context context;
    List<TimeSlot> timeSlots;
    List<CardView> cardViewList;
    LocalBroadcastManager localBroadcastManager;

    public TimeSlotAdapter(Context context) {
        this.context = context;
        this.timeSlots = new ArrayList<>();
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        cardViewList = new ArrayList<>();
    }

    public TimeSlotAdapter(Context context, List<TimeSlot> timeSlots) {
        this.context = context;
        this.timeSlots = timeSlots;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        cardViewList = new ArrayList<>();
    }

    @NonNull
    @Override
    public TimeSlotViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_time_slot,viewGroup,false);
        return new TimeSlotViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TimeSlotViewHolder holder, int position) {
        holder.txt_time.setText(new StringBuilder(Common.convertTimeSlotToString(position)).toString());
        if (timeSlots.size() == 0) // if all positions are available just show list
        {
            holder.txt_time_desc.setText(new StringBuilder("Available") );
            holder.txt_time_desc.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.txt_time.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
        }
        else // if time slot already picked
        {
            for (TimeSlot slotVal : timeSlots)
            {
                //Loop all time slots from server and set different color
                int slot = Integer.parseInt(slotVal.getSlot().toString());
                if (slot == position) // if slot == position
                {
                    //set a tag for all time slots that are full
                    //based on the tag we can set all remaining card backgrounds without changing the full time slots]
                    holder.time_card.setTag(Common.DISABLE_TAG);

                    holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));

                    holder.txt_time_desc.setText("FULL");
                    holder.txt_time_desc.setTextColor(context.getResources().getColor(android.R.color.white));
                    holder.txt_time.setTextColor(context.getResources().getColor(android.R.color.white));
                }
            }
        }

        //Add all time slots to the list (20 cards)
        //don't add card if it's already in the lsit
        if (!cardViewList.contains(holder.time_card))
            cardViewList.add(holder.time_card);

        //Check if time_slot is available

            holder.setiItemSelectedListener((view, position1) -> {
                //Loop through all cards in the list
                for (CardView cardView:cardViewList){
                    if (cardView.getTag() == null) //only available time slot will be changed
                        cardView.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
                }

                //Our selected card will change color
                holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.holo_orange_dark));

                //Send Broadcast to enable Button Next
                Intent intent = new Intent(Common.KEY_ENABLE_BUTTON_NEXT);
                intent.putExtra(Common.KEY_TIME_SLOT, position1); //put index of time slot selected
                intent.putExtra(Common.KEY_STEP,3); //Go to step 3
                localBroadcastManager.sendBroadcast(intent);
            });

    }

    @Override
    public int getItemCount() {
        return Common.TIME_SLOT_TOTAL;
    }

    public class TimeSlotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_time,txt_time_desc;
        CardView time_card;

        IItemSelectedListener iItemSelectedListener;

        public void setiItemSelectedListener(IItemSelectedListener iItemSelectedListener) {
            this.iItemSelectedListener = iItemSelectedListener;
        }

        public TimeSlotViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_time = itemView.findViewById(R.id.txt_time_slot);
            txt_time_desc = itemView.findViewById(R.id.time_slot_description);
            time_card = itemView.findViewById(R.id.time_slot_card);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iItemSelectedListener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
