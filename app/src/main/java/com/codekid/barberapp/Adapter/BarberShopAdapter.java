package com.codekid.barberapp.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Interface.IItemSelectedListener;
import com.codekid.barberapp.Models.BarberShops;
import com.codekid.barberapp.R;

import java.util.ArrayList;
import java.util.List;

public class BarberShopAdapter  extends RecyclerView.Adapter<BarberShopAdapter.BarbershopViewHolder> {

    Context mContext;
    List<BarberShops> shopsList;
    List<CardView> cardViewList;
    LocalBroadcastManager localBroadcastManager;

    public BarberShopAdapter(Context mContext, List<BarberShops> shopsList) {
        this.mContext = mContext;
        this.shopsList = shopsList;
        cardViewList = new ArrayList<>();
        localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
    }

    @NonNull
    @Override
    public BarbershopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_barber_shop,viewGroup,false);
        return new BarbershopViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final BarbershopViewHolder holder, int i) {
        holder.txt_shop_name.setText(shopsList.get(i).getName());
        holder.txt_shop_address.setText(shopsList.get(i).getAddress());
        if (!cardViewList.contains(holder.barber_card))
            cardViewList.add(holder.barber_card);

        holder.setiItemSelectedListener((view, position) -> {
            //Set White Background for all cards not selected
            for (CardView cardView:cardViewList)
                cardView.setCardBackgroundColor(mContext.getResources().getColor(android.R.color.white));

            //Set selected BG for only selected items
            holder.barber_card.setCardBackgroundColor(mContext.getResources().getColor(android.R.color.holo_orange_dark));

            //Send Broadcast to Booking Activity to enable Button Next
            Intent intent = new Intent(Common.KEY_ENABLE_BUTTON_NEXT);
            intent.putExtra(Common.KEY_BARBER_STORE,shopsList.get(position));
            intent.putExtra(Common.KEY_STEP,1);
            localBroadcastManager.sendBroadcast(intent);

        });
    }

    @Override
    public int getItemCount() {
        return shopsList.size();
    }

    public class BarbershopViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_shop_name,txt_shop_address;
        CardView barber_card;

        IItemSelectedListener iItemSelectedListener;

        public void setiItemSelectedListener(IItemSelectedListener iItemSelectedListener) {
            this.iItemSelectedListener = iItemSelectedListener;
        }

        public BarbershopViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_shop_name = itemView.findViewById(R.id.shop_name);
            txt_shop_address = itemView.findViewById(R.id.shop_address);
            barber_card = itemView.findViewById(R.id.barbershop_card);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iItemSelectedListener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
