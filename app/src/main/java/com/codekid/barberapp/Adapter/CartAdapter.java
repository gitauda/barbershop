package com.codekid.barberapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codekid.barberapp.Database.CartDatabase;
import com.codekid.barberapp.Database.CartItem;
import com.codekid.barberapp.Database.DatabaseUtils;
import com.codekid.barberapp.Interface.ICartItemUpdateListener;
import com.codekid.barberapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    Context context;
    List<CartItem> cartItems;
    CartDatabase cartDatabase;
    ICartItemUpdateListener itemUpdateListener;

    public CartAdapter(Context context, List<CartItem> cartItems, ICartItemUpdateListener itemUpdateListener) {
        this.context = context;
        this.cartItems = cartItems;
        this.itemUpdateListener = itemUpdateListener;
        this.cartDatabase = CartDatabase.getInstance(context);
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.cart_item_layout,viewGroup,false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder holder, int pos) {
        Picasso.get().load(cartItems.get(pos).getProductImage()).into(holder.product_img);
        holder.txt_cart_name.setText(cartItems.get(pos).getProductName());
        holder.txt_cart_price.setText(new StringBuilder("KES ").append(cartItems.get(pos).getProductPrice()));
        holder.txt_quantity.setText(new StringBuilder(String.valueOf(cartItems.get(pos).getProductQuantity())));

        //Event
        holder.setListener(new IImageButtonListener() {
            @Override
            public void onImageButtonClick(View view, int pos, boolean isDecrease) {
                if (isDecrease)
                {
                    if (cartItems.get(pos).getProductQuantity() > 0)
                    {
                        cartItems.get(pos)
                                .setProductQuantity(cartItems.get(pos).getProductQuantity()-1);
                        DatabaseUtils.updateCart(cartDatabase,cartItems.get(pos));
                    }
                    }
                else
                {
                    if (cartItems.get(pos).getProductQuantity() < 99)
                    {
                        cartItems.get(pos)
                                .setProductQuantity(cartItems.get(pos).getProductQuantity()+1);
                        DatabaseUtils.updateCart(cartDatabase,cartItems.get(pos));
                    }
                }

                holder.txt_quantity.setText(new StringBuilder(String.valueOf(cartItems.get(pos).getProductQuantity())));
                itemUpdateListener.onCartItemUpdateSuccess();

            }
        });
    }

    @Override
    public int getItemCount() {
        return  cartItems.size();
    }

    interface IImageButtonListener{
        void onImageButtonClick(View view,int pos,boolean isDecrease);
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{

        TextView txt_cart_name,txt_cart_price,txt_quantity;
        ImageView img_decrease,img_increase,product_img;

        IImageButtonListener listener;

        public void setListener(IImageButtonListener listener) {
            this.listener = listener;
        }

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_cart_name = itemView.findViewById(R.id.cart_name);
            txt_cart_price = itemView.findViewById(R.id.cart_price);
            txt_quantity = itemView.findViewById(R.id.cart_qty);
            img_decrease = itemView.findViewById(R.id.decrease_img);
            img_increase = itemView.findViewById(R.id.increase_img);
            product_img = itemView.findViewById(R.id.cart_img);

            //Event
            img_decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onImageButtonClick(v,getAdapterPosition(),true);
                }
            });

            img_increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onImageButtonClick(v,getAdapterPosition(),false);
                }
            });
        }
    }
}
