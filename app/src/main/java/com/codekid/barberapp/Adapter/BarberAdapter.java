package com.codekid.barberapp.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Interface.IItemSelectedListener;
import com.codekid.barberapp.Models.Barber;
import com.codekid.barberapp.R;

import java.util.ArrayList;
import java.util.List;

public class BarberAdapter extends RecyclerView.Adapter<BarberAdapter.BarberViewHolder> {

    Context mContext;
    List<Barber> barbers;
    List<CardView> cardViewList;
    LocalBroadcastManager localBroadcastManager;

    public BarberAdapter(Context mContext, List<Barber> barbers) {
        this.mContext = mContext;
        this.barbers = barbers;
        cardViewList = new ArrayList<>();
        localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
    }

    @NonNull
    @Override
    public BarberViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_barber,viewGroup,false);
        return new BarberViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final BarberViewHolder holder, final int pos) {
        holder.barber_name.setText(barbers.get(pos).getName());
        holder.barber_rating.setRating((float)barbers.get(pos).getRating());
        if (!cardViewList.contains(holder.barber_card))
            cardViewList.add(holder.barber_card);

        holder.setListener((view, position) -> {
            //Set Background for all items not chosen
            for (CardView cardView : cardViewList){
                cardView.setCardBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            }

            //Background for selected cards
            holder.barber_card.setCardBackgroundColor(mContext.getResources()
            .getColor(android.R.color.holo_orange_dark));

            //send local broadcast to enable button next
            Intent intent = new Intent(Common.KEY_ENABLE_BUTTON_NEXT);
            intent.putExtra(Common.KEY_BARBER_SELECTED,barbers.get(pos));
            intent.putExtra(Common.KEY_STEP , 2);
            localBroadcastManager.sendBroadcast(intent);

        });
    }

    @Override
    public int getItemCount() {
        return barbers.size();
    }

    public class BarberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView barber_image;
        TextView barber_name;
        RatingBar barber_rating;
        CardView barber_card;

        IItemSelectedListener listener;

        public void setListener(IItemSelectedListener listener) {
            this.listener = listener;
        }

        public BarberViewHolder(@NonNull View itemView) {
            super(itemView);
            barber_image = itemView.findViewById(R.id.barber_image);
            barber_name = itemView.findViewById(R.id.barber_name);
            barber_rating = itemView.findViewById(R.id.barber_rating);
            barber_card = itemView.findViewById(R.id.barber_card);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
