package com.codekid.barberapp.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Database.CartDatabase;
import com.codekid.barberapp.Database.CartItem;
import com.codekid.barberapp.Database.DatabaseUtils;
import com.codekid.barberapp.Interface.IItemSelectedListener;
import com.codekid.barberapp.Models.ShoppingItem;
import com.codekid.barberapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingAdapter.ShoppingViewHolder> {

    Context context;
    List<ShoppingItem> shoppingItems;
    CartDatabase cartDatabase;

    public ShoppingAdapter(Context context, List<ShoppingItem> shoppingItems) {
        this.context = context;
        this.shoppingItems = shoppingItems;
        cartDatabase = CartDatabase.getInstance(context);
    }

    @NonNull
    @Override
    public ShoppingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.shopping_layout,viewGroup,false);
        return new ShoppingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingViewHolder holder, int i) {
        Picasso.get().load(shoppingItems.get(i).getImage()).into(holder.item_image);

        holder.item_price.setText(new StringBuilder("KES ").append(shoppingItems.get(i).getPrice()).toString());
        holder.item_name.setText(Common.formatShoppingItemName(shoppingItems.get(i).getName()));

        //Add To Cart
        holder.setItemSelectedListener((view, position) -> {
            //Create Cart Item
            CartItem cartItem = new CartItem();
            cartItem.setProductId(shoppingItems.get(position).getId());
            cartItem.setProductName(shoppingItems.get(position).getName());
            cartItem.setProductPrice(shoppingItems.get(position).getPrice());
            cartItem.setProductImage(shoppingItems.get(position).getImage());
            cartItem.setUserPhone(Common.currentUser.getPhoneNumber());
            cartItem.setProductQuantity(1);

            //Insert to Db
            DatabaseUtils.insertToCart(cartDatabase,cartItem);
            Toast.makeText(context, "Added To Cart", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return shoppingItems.size();
    }

    public class ShoppingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView item_name,item_price,add_to_cart;
        ImageView item_image;

        IItemSelectedListener itemSelectedListener;

        public void setItemSelectedListener(IItemSelectedListener itemSelectedListener) {
            this.itemSelectedListener = itemSelectedListener;
        }

        public ShoppingViewHolder(@NonNull View itemView) {
            super(itemView);
            item_image = itemView.findViewById(R.id.shopping_item_img);
            item_name = itemView.findViewById(R.id.txt_shop_item_name);
            item_price = itemView.findViewById(R.id.txt_shop_item_price);
            add_to_cart = itemView.findViewById(R.id.txt_add_to_cart);

            add_to_cart.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemSelectedListener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
