package com.codekid.barberapp.Database;

import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;

import com.codekid.barberapp.CartActivity;
import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Interface.ICartItemCountListener;
import com.codekid.barberapp.Interface.IcartItemLoadListener;
import com.codekid.barberapp.Interface.SumCartListener;

import java.util.List;

public class DatabaseUtils {
    //Because  room handles all other work in different thread

    public static void insertToCart(CartDatabase db,CartItem...cartItems) {
        InsertToCartAsync task = new InsertToCartAsync(db);
        task.execute(cartItems);
    }

    public static void countCartItems(CartDatabase db, ICartItemCountListener itemCountListener) {
        CountCartItemAsync task = new CountCartItemAsync(db,itemCountListener);
        task.execute();
    }

    public static void getCart(CartDatabase db, IcartItemLoadListener icartItemLoadListener) {
        GetAllCartAsync task = new GetAllCartAsync(db,icartItemLoadListener);
        task.execute();
    }

    public static void updateCart(CartDatabase cartDatabase, CartItem cart) {
        UpdateCartAsync task = new UpdateCartAsync(cartDatabase);
        task.execute(cart);
    }

    public static void sumCart(CartDatabase db, SumCartListener cartListener) {
        SumCartAsync task = new SumCartAsync(db,cartListener);
        task.execute();
    }

    /*
    ====================================================================
    DEFINE ASYNC TASK
    ====================================================================
     */

    private static class GetAllCartAsync extends AsyncTask<String,Void,List<CartItem>>{

        CartDatabase db;
        IcartItemLoadListener listener;
        public GetAllCartAsync(CartDatabase cartDatabase,IcartItemLoadListener icartItemLoadListener)
        {
            db = cartDatabase;
            listener = icartItemLoadListener;
        }

        @Override
        protected List<CartItem> doInBackground(String... strings) {
            return db.cartDAO().getAllItemsFromCart(Common.currentUser.getPhoneNumber());
        }

        @Override
        protected void onPostExecute(List<CartItem> cartItems) {
            super.onPostExecute(cartItems);
            listener.onGetAllItemsFromCartSuccess(cartItems);
        }
    }

    private static class InsertToCartAsync extends AsyncTask<CartItem,Void,Void>{

        CartDatabase db;
        public InsertToCartAsync(CartDatabase cartDatabase)
        {
            db = cartDatabase;
        }

        @Override
        protected Void doInBackground(CartItem... cartItems) {
            insertToCart(db,cartItems[0]);
            return null;
        }

        private void insertToCart(CartDatabase db, CartItem cartItem) {
            //If item is already in cart ,just increase quantity
            try {
                db.cartDAO().insert(cartItem);

            }catch (SQLiteConstraintException exception)
            {
                CartItem updateCartItem = db.cartDAO().getProductsInCart(cartItem.getProductId(),
                        Common.currentUser.getPhoneNumber());
                updateCartItem.setProductQuantity(updateCartItem.getProductQuantity()+1);
                db.cartDAO().update(updateCartItem);
            }
        }


    }

    private static class CountCartItemAsync extends AsyncTask<Void,Void,Integer>{

        CartDatabase db;
        ICartItemCountListener listener;
        public CountCartItemAsync(CartDatabase cartDatabase,ICartItemCountListener iCartItemCountListener)
        {
            db = cartDatabase;
            listener = iCartItemCountListener;
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            return Integer.parseInt(String.valueOf(countItemsInCart(db)));
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            listener.onCartItemCountSuccess(integer.intValue());
        }

        private int countItemsInCart(CartDatabase db) {
            return db.cartDAO().countCartitems(Common.currentUser.getPhoneNumber());
        }
    }

    private static class UpdateCartAsync extends AsyncTask<CartItem,Void,Void> {
        private final CartDatabase db;

        public UpdateCartAsync(CartDatabase db)
        {
            this.db = db;
        }

        @Override
        protected Void doInBackground(CartItem... cartItems) {
            db.cartDAO().update(cartItems[0]);
            return null;
        }
    }

    private static class SumCartAsync extends AsyncTask<Void,Void,Long> {
        private final CartDatabase db;
        private final SumCartListener listener;

        public SumCartAsync(CartDatabase db,SumCartListener sumCartListener )
        {
            this.db = db;
            this.listener = sumCartListener;
        }

        @Override
        protected Long doInBackground(Void... voids) {
            return sumInCart(db);
        }

        private Long sumInCart(CartDatabase db) {
            return db.cartDAO().sumPrice(Common.currentUser.getPhoneNumber());
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            listener.onSumCartListenerSuccess(aLong);
        }
    }
}
