package com.codekid.barberapp.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CartDAO {
    @Query("SELECT * FROM Cart WHERE userPhone=:userPhone")
    List<CartItem> getAllItemsFromCart(String userPhone);

    @Query("SELECT  COUNT(*) from Cart Where userPhone=:userPhone")
    int countCartitems(String userPhone);

    @Query("SELECT * from Cart where productId=:productId AND userPhone=:userPhone")
    CartItem getProductsInCart(String productId,String userPhone);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(CartItem...carts);

    @Update(onConflict = OnConflictStrategy.FAIL)
    void update(CartItem cart);

    @Delete
    void delete(CartItem cartItem);

    @Query("DELETE FROM Cart WHERE userPhone=:userPhone")
    void clearCart(String userPhone);

    @Query("SELECT SUM(productPrice*productQuantity) from Cart where userPhone=:userPhone")
    Long sumPrice(String userPhone);
}
