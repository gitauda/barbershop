package com.codekid.barberapp.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.codekid.barberapp.Common.Common;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import io.paperdb.Paper;

public class FCMService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Common.updateToken(this,s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);



        //dataSend.put("update_done","true")
        if (remoteMessage.getData() != null)
        {
            if (remoteMessage.getData().get("update_done") != null)
            {
                updateLastBooking();

                Map<String,String> dataReceived = remoteMessage.getData();
                Paper.init(this);
                Paper.book().write(Common.RATING_INFO_KEY,new Gson().toJson(dataReceived));
            }

            if (remoteMessage.getData().get(Common.TITLE_KEY) != null &&
                    remoteMessage.getData().get(Common.CONTENT_KEY) != null)
            {
                Common.showNotification(this,new Random().nextInt(),
                        remoteMessage.getData().get(Common.TITLE_KEY),
                        remoteMessage.getData().get(Common.CONTENT_KEY),
                        null);
            }
        }
    }

    private void updateLastBooking() {
        //Here we need to get current user login
        //Because the app may be running on the background we need to get from paper

        CollectionReference userBooking;

        //if app is running
        if (Common.currentUser != null)
        {
            userBooking = FirebaseFirestore.getInstance()
                    .collection("Users")
                    .document(Common.currentUser.getPhoneNumber())
                    .collection("Booking");
        }
        else
        {
            //if the app isn't running
            Paper.init(this);
            String user = Paper.book().read(Common.LOGGED_KEY);

            userBooking = FirebaseFirestore.getInstance()
                    .collection("Users")
                    .document(user)
                    .collection("Booking");

        }

        //check if exists by current date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,0);
        calendar.add(Calendar.HOUR_OF_DAY,0);
        calendar.add(Calendar.MINUTE,0);

        Timestamp timestamp = new Timestamp(calendar.getTimeInMillis());
        CollectionReference finalUserBooking;
        userBooking
                .whereGreaterThanOrEqualTo("timestamp",timestamp)//get only booking info for today and next three days
                .whereEqualTo("done",false)  //and done is false
                .limit(1)
                .get()
                .addOnFailureListener(e -> Toast.makeText(FCMService.this, e.getMessage(), Toast.LENGTH_SHORT).show())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        if (task.getResult().size() > 0)
                        {
                            //Update
                            DocumentReference currentUserBooking = null;
                            for (DocumentSnapshot snapshot: task.getResult())
                            {
                                currentUserBooking = userBooking.document(snapshot.getId());

                            }
                            if (currentUserBooking != null)
                            {
                                Map<String,Object> dataUpdate = new HashMap<>();
                                dataUpdate.put("done",true);
                                currentUserBooking.update(dataUpdate)
                                        .addOnFailureListener(e -> Toast.makeText(FCMService.this, e.getMessage(), Toast.LENGTH_SHORT).show());
                            }
                        }
                    }
                });
    }
}
