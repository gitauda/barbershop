package com.codekid.barberapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;

import com.codekid.barberapp.Adapter.ViewPagerAdapter;
import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Common.NonSwipeViewPager;
import com.codekid.barberapp.Models.Barber;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.shuhart.stepview.StepView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;

public class BookingActivity extends AppCompatActivity {

    LocalBroadcastManager localBroadcastManager;
    AlertDialog alertDialog;
    CollectionReference barberRef;

    @BindView(R.id.step_view)
    StepView stepView;
    @BindView(R.id.view_pager)
    NonSwipeViewPager viewPager;
    @BindView(R.id.btn_prev)
    Button btn_prev;
    @BindView(R.id.btn_next)
    Button btn_next;

    //Event
    @OnClick(R.id.btn_prev)
    void prevStep()
    {
        if(Common.step == 3 || Common.step > 0)
        {
            Common.step--;
            viewPager.setCurrentItem(Common.step);
            if (Common.step < 3) //Always enable NEXT when step <3
            {
                btn_next.setEnabled(true);
                setButtonColors();
            }
        }
    }

    @OnClick(R.id.btn_next)
    void nextClick()
    {
        if (Common.step < 3 || Common.step == 0)
        {
            Common.step++; //increase
            if (Common.step == 1) //after choosing shop
            {
                if (Common.currentShop != null)
                    loadBarberByShop(Common.currentShop.getShopId());
            }
            else if(Common.step ==2) //Pick Time Slot
            {
                if(Common.currentBarber != null)
                    loadTimeSlot(Common.currentBarber.getBarberId());

            }
            else if(Common.step == 3) //Pick Confirm
            {
                if(Common.currentTimeSlot != -1)
                    confirmBooking();

            }
            viewPager.setCurrentItem(Common.step);
        }
    }

    private void confirmBooking() {
        //Send Broadcast to Fragment 4
        Intent intent = new Intent(Common.KEY_CONFIRM_BOOKING);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void loadTimeSlot(String barberId) {
        //Send Local Broadcast to Step 3 Fragment
        Intent intent = new Intent(Common.KEY_DISPLAY_TIME_SLOT);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void loadBarberByShop(String shopId) {
        alertDialog.show();

        //Now , Select all barbers from shop
        ///BarberShops/Nairobi/Branch/brv6UA3HkpP1H309IwGS/Barbers/HGSQKWy32cN3FvEDDytt
        if (!TextUtils.isEmpty(Common.city))
        {
            barberRef = FirebaseFirestore.getInstance()
                    .collection("BarberShops")
                    .document(Common.city)
                    .collection("Branch")
                    .document(shopId)
                    .collection("Barbers");

            barberRef.get()
                    .addOnCompleteListener(task -> {
                        ArrayList<Barber> barbers = new ArrayList<>();
                        for (QueryDocumentSnapshot barberSnap:task.getResult())
                        {
                            Barber barber = barberSnap.toObject(Barber.class);
                            barber.setPassword(""); // Remove password because this is client app
                            barber.setBarberId(barberSnap.getId());

                            barbers.add(barber);
                        }

                        //Send Broadcast to BookingFragment2 to load Recycler
                        Intent intent = new Intent(Common.KEY_BARBER_LOAD_DONE);
                        intent.putParcelableArrayListExtra(Common.KEY_BARBER_LOAD_DONE,barbers);
                        localBroadcastManager.sendBroadcast(intent);

                        alertDialog.dismiss();

                    })
                    .addOnFailureListener(e -> alertDialog.dismiss());
        }

    }

    //BroadCast Receiver
    private BroadcastReceiver btnNextReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int step = intent.getIntExtra(Common.KEY_STEP,0);
            if (step == 1)
                Common.currentShop = intent.getParcelableExtra(Common.KEY_BARBER_STORE);
            else if (step == 2)
                Common.currentBarber = intent.getParcelableExtra(Common.KEY_BARBER_SELECTED);
            else if (step == 3)
                Common.currentTimeSlot = intent.getIntExtra(Common.KEY_TIME_SLOT,-1);

            btn_next.setEnabled(true);
            setButtonColors();
        }
    };

    @Override
    protected void onDestroy() {
        localBroadcastManager.unregisterReceiver(btnNextReceiver);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);

        alertDialog = new SpotsDialog.Builder().setContext(this).setCancelable(false).build();

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(btnNextReceiver,new IntentFilter(Common.KEY_ENABLE_BUTTON_NEXT));

        setUpStepView();
        setButtonColors();

        //View
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(4); //we have 4 fragments so we need to keep the state of these 4 screen page
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                //Show steps
                stepView.go(i,true);

                if ( i == 0)
                    btn_prev.setEnabled(false);
                else
                    btn_prev.setEnabled(true);

                //set button next to disabled
                btn_next.setEnabled(false);

                setButtonColors();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setButtonColors() {
        if (btn_next.isEnabled())
        {
            btn_next.setBackgroundResource(R.color.buttonColor);
        }
        else
        {
            btn_next.setBackgroundResource(android.R.color.darker_gray);
        }

        if (btn_prev.isEnabled())
        {
            btn_prev.setBackgroundResource(R.color.buttonColor);
        }
        else
        {
            btn_prev.setBackgroundResource(android.R.color.darker_gray);
        }
    }

    private void setUpStepView() {
        List<String> stepList = new ArrayList<>();
        stepList.add("BarberShops");
        stepList.add("Barber");
        stepList.add("Time");
        stepList.add("confirm");
        stepView.setSteps(stepList);
    }
}
