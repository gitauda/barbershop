package com.codekid.barberapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import com.codekid.barberapp.Adapter.CartAdapter;
import com.codekid.barberapp.Database.CartDatabase;
import com.codekid.barberapp.Database.CartItem;
import com.codekid.barberapp.Database.DatabaseUtils;
import com.codekid.barberapp.Interface.ICartItemUpdateListener;
import com.codekid.barberapp.Interface.IcartItemLoadListener;
import com.codekid.barberapp.Interface.SumCartListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends AppCompatActivity implements IcartItemLoadListener, ICartItemUpdateListener, SumCartListener {

    @BindView(R.id.cart_recycler)
    RecyclerView cart_recycler;
    @BindView(R.id.txt_total_price)
    TextView txt_total_price;
    @BindView(R.id.btn_submit_cart)
    Button btn_submit_cart;

    CartDatabase cartDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        ButterKnife.bind(this);

        cartDatabase = CartDatabase.getInstance(this);

        DatabaseUtils.getCart(cartDatabase,this);

        //View
        cart_recycler.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        cart_recycler.setLayoutManager(linearLayoutManager);
        cart_recycler.addItemDecoration(new DividerItemDecoration(this,linearLayoutManager.getOrientation()));
    }


    @Override
    public void onGetAllItemsFromCartSuccess(List<CartItem> cartItems) {
        //Here after we get all items from db
        // we will display them in the recycler view
        CartAdapter adapter = new CartAdapter(this,cartItems,this);
        cart_recycler.setAdapter(adapter);
    }

    @Override
    public void onCartItemUpdateSuccess() {
        DatabaseUtils.sumCart(cartDatabase,this);
    }

    @Override
    public void onSumCartListenerSuccess(Long value) {
        txt_total_price.setText(new StringBuilder("KES ").append(value));
    }
}
