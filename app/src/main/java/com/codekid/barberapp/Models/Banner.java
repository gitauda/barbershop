package com.codekid.barberapp.Models;

public class Banner {
    //Look book and banner is the same
    private String image;

    public Banner() {
    }

    public Banner(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
