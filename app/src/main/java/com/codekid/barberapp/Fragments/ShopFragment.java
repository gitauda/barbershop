package com.codekid.barberapp.Fragments;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codekid.barberapp.Adapter.ShoppingAdapter;
import com.codekid.barberapp.Common.SpaceItemDecoration;
import com.codekid.barberapp.Interface.IShoppingItemLoadListener;
import com.codekid.barberapp.Models.ShoppingItem;
import com.codekid.barberapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment implements IShoppingItemLoadListener {

    CollectionReference shoppingItemRef;

    Unbinder unbinder;

    IShoppingItemLoadListener shoppingItemLoadListener;

    @BindView(R.id.chip_group)
    ChipGroup chipGroup;
    @BindView(R.id.chip_wax)
    Chip wax_chip;
    @OnClick(R.id.chip_wax)
    void waxChipClick()
    {
        setSelectedChip(wax_chip);
        loadShoppingItems("Wax");
    }
    @BindView(R.id.chip_spray)
    Chip spray_chip;
    @OnClick(R.id.chip_spray)
    void sprayChipClick()
    {
        setSelectedChip(spray_chip);
        loadShoppingItems("Spray");
    }
    @BindView(R.id.chip_body_care)
    Chip body_care_chip;
    @OnClick(R.id.chip_body_care)
    void bodyChipClick()
    {
        setSelectedChip(body_care_chip);
        loadShoppingItems("Body_Care");
    }
    @BindView(R.id.chip_hair_care)
    Chip hair_care_chip;
    @OnClick(R.id.chip_hair_care)
    void hairChipClick()
    {
        setSelectedChip(hair_care_chip);
        loadShoppingItems("Hair_Care");
    }

    @BindView(R.id.items_recycler)
    RecyclerView shop_items_recycler;

    private void loadShoppingItems(String menuItem) {
        shoppingItemRef = FirebaseFirestore.getInstance()
                .collection("Shopping")
                .document(menuItem)
                .collection("items");

        //Get Data
        shoppingItemRef.get()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        shoppingItemLoadListener.onShoppingItemLoadFailure(e.getMessage());
                    }
                }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful())
                {
                    List<ShoppingItem> shoppingItems = new ArrayList<>();
                    for (DocumentSnapshot snapshot: task.getResult())
                    {
                        ShoppingItem shoppingItem = snapshot.toObject(ShoppingItem.class);
                        shoppingItem.setId(snapshot.getId()); //add this to get product Id
                        shoppingItems.add(shoppingItem);
                    }
                    shoppingItemLoadListener.onShoppingItemLoadSuccess(shoppingItems);
                }
            }
        });
    }

    private void setSelectedChip(Chip wax_chip) {
        //Set Color
        for (int i=0;i<chipGroup.getChildCount();i++)
        {
            Chip chipItem = (Chip) chipGroup.getChildAt(i);
            if (chipItem.getId() != wax_chip.getId()) // if not Selected
            {
                chipItem.setChipBackgroundColorResource(android.R.color.darker_gray);
                chipItem.setTextColor(getResources().getColor(android.R.color.white));
            }
            else // if Selected
            {
                chipItem.setChipBackgroundColorResource(android.R.color.holo_orange_dark);
                chipItem.setTextColor(getResources().getColor(android.R.color.black));

            }
        }
    }

    public ShopFragment() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View itemView = inflater.inflate(R.layout.fragment_shop, container, false);

        unbinder = ButterKnife.bind(this,itemView);

        shoppingItemLoadListener = this;

        //Default Load
        loadShoppingItems("Wax");

        initView();

        return  itemView;
    }

    private void initView() {
        shop_items_recycler.setHasFixedSize(true);
        shop_items_recycler.setLayoutManager(new GridLayoutManager(getContext(),2));
        shop_items_recycler.addItemDecoration(new SpaceItemDecoration(8));
    }

    @Override
    public void onShoppingItemLoadSuccess(List<ShoppingItem> shoppingItems) {
        ShoppingAdapter adapter = new ShoppingAdapter(getContext(),shoppingItems);
        shop_items_recycler.setAdapter(adapter);
    }

    @Override
    public void onShoppingItemLoadFailure(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
