package com.codekid.barberapp.Fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codekid.barberapp.Adapter.BarberShopAdapter;
import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Common.SpaceItemDecoration;
import com.codekid.barberapp.Interface.IBarberShopLoadListener;
import com.codekid.barberapp.Interface.IBranchLoadListener;
import com.codekid.barberapp.Models.BarberShops;
import com.codekid.barberapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class BookingStep1Fragment extends Fragment implements IBarberShopLoadListener, IBranchLoadListener {


    //Variables
    CollectionReference barbershopRef;
    CollectionReference branchRef;

    IBarberShopLoadListener shopLoadListener;
    IBranchLoadListener branchLoadListener;

    @BindView(R.id.spinner)
    MaterialSpinner spinner;
    @BindView(R.id.barbershop_recycler)
    RecyclerView shop_recycler;

    Unbinder unbinder;

    AlertDialog dialog;

    static BookingStep1Fragment instance;

    public static BookingStep1Fragment getInstance(){
        if (instance == null)
            instance = new BookingStep1Fragment();
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        barbershopRef = FirebaseFirestore.getInstance().collection("BarberShops");
        shopLoadListener = this;
        branchLoadListener = this;

        dialog = new SpotsDialog.Builder().setContext(getActivity()).setCancelable(false).build();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);

         View itemView = inflater.inflate(R.layout.fragment_booking_step_one,container,false);
         unbinder = ButterKnife.bind(this,itemView);

         initView();
         loadAllBarberShops();

         return itemView;

    }

    private void initView() {
        shop_recycler.setHasFixedSize(true);
        shop_recycler.setLayoutManager(new GridLayoutManager(getActivity(),2));
        shop_recycler.addItemDecoration(new SpaceItemDecoration(4));
    }

    private void loadAllBarberShops() {
        barbershopRef.get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        List<String> list = new ArrayList<>();
                        list.add("Please Choose A City");
                        for(QueryDocumentSnapshot documentSnapshot:task.getResult())
                            list.add(documentSnapshot.getId());
                        shopLoadListener.onBarberShopLoadSuccess(list);

                    }
                }).addOnFailureListener(e -> shopLoadListener.onBarberShopLoadFailed(e.getMessage()));
    }

    @Override
    public void onBarberShopLoadSuccess(List<String> areaNameList) {
        spinner.setItems(areaNameList);
        spinner.setOnItemSelectedListener((view, position, id, item) -> {
            if (position > 0)
            {
                loadBranchAsPerCity(item.toString());
            }
            else
                shop_recycler.setVisibility(View.GONE);

        });
    }

    private void loadBranchAsPerCity(String cityName) {
        dialog.show();

        Common.city = cityName;

        branchRef = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(cityName)
                .collection("Branch");

        branchRef.get()
                .addOnCompleteListener(task -> {
                   List<BarberShops> shops = new ArrayList<>();
                   if (task.isSuccessful())
                   {
                       for (QueryDocumentSnapshot snapshot:task.getResult())
                       {
                           BarberShops barberShops = snapshot.toObject(BarberShops.class);
                           barberShops.setShopId(snapshot.getId());
                           shops.add(barberShops);
                       }
                       branchLoadListener.onBranchLoadSuccess(shops);
                   }
                }).addOnFailureListener(e -> branchLoadListener.onBranchLoadFailure(e.getMessage()));
    }

    @Override
    public void onBarberShopLoadFailed(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBranchLoadSuccess(List<BarberShops> shopsList) {
        BarberShopAdapter adapter = new BarberShopAdapter(getActivity(),shopsList);
        shop_recycler.setAdapter(adapter);
        shop_recycler.setVisibility(View.VISIBLE);

        dialog.dismiss();
    }

    @Override
    public void onBranchLoadFailure(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }
}
