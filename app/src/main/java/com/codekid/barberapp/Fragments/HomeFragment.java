package com.codekid.barberapp.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberapp.Adapter.HomeSliderAdapter;
import com.codekid.barberapp.Adapter.LookBookAdapter;
import com.codekid.barberapp.BookingActivity;
import com.codekid.barberapp.CartActivity;
import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Database.CartDatabase;
import com.codekid.barberapp.Database.DatabaseUtils;
import com.codekid.barberapp.Interface.IBannerLoadListener;
import com.codekid.barberapp.Interface.IBookingInfoChangeListener;
import com.codekid.barberapp.Interface.IBookingInfoLoadListener;
import com.codekid.barberapp.Interface.ICartItemCountListener;
import com.codekid.barberapp.Interface.ILookBookListener;
import com.codekid.barberapp.Models.Banner;
import com.codekid.barberapp.Models.BookingInfo;
import com.codekid.barberapp.R;
import com.codekid.barberapp.Service.PicassoImageLoadingService;
import com.facebook.accountkit.AccountKit;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;
import ss.com.bannerslider.Slider;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ILookBookListener, IBannerLoadListener, IBookingInfoLoadListener, IBookingInfoChangeListener, ICartItemCountListener {

    private Unbinder unbinder;

    CartDatabase database;

    AlertDialog dialog;


    @BindView(R.id.notification_badge)
    NotificationBadge notificationBadge;

    @BindView(R.id.layout_user_info)
    LinearLayout user_inf_layout;
    @BindView(R.id.txt_user_name)
    TextView txt_user_name;
    @BindView(R.id.banner_slider)
    Slider banner_slider;
    @BindView(R.id.look_book_recycler)
    RecyclerView look_book_recycler;

    @BindView(R.id.booking_info)
    CardView booking_card;
    @BindView(R.id.txt_bShop_address)
    TextView shop_address;
    @BindView(R.id.txt_barber)
    TextView barber_selected;
    @BindView(R.id.txt_time)
    TextView time_selected;
    @BindView(R.id.txt_remaining_time)
    TextView remaining_time;
    @OnClick(R.id.booking_card)
    void booking()
    {
        startActivity(new Intent(getActivity(), BookingActivity.class));
    }
    @OnClick(R.id.booking_cart)
    void openCart()
    {
        startActivity(new Intent(getActivity(), CartActivity.class));
    }


    @OnClick(R.id.btn_delete)
    void deleteBooking()
    {
        deleteBookingFromBarber(false);
    }

    @OnClick(R.id.btn_change)
    void changeBooking()
    {
        changeBookingFromUser();
    }

    private void changeBookingFromUser() {
        //Show Dialog
        androidx.appcompat.app.AlertDialog.Builder confirmDialog = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        confirmDialog.setCancelable(false);
        confirmDialog.setTitle("HEY");
        confirmDialog.setMessage("Do You Really Want to change booking Information\n because we will delete your old Appointment\n just confirm");
        confirmDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteBookingFromBarber(true); // true because we called from button Change
            }
        });

        confirmDialog.show();

    }

    private void deleteBookingFromBarber(final boolean isChange) {
        /*
        To delete booking, first we must delete from barber collections
        After that, we will delete from User booking collections
        and then finally delete the event
         */

        //We need load the currentBooking because we need some data from bookingInformation
        if (Common.currentBooking != null)
        {
            dialog.show();

            //Get booking information in barber object
            DocumentReference barberbooking = FirebaseFirestore.getInstance()
                    .collection("BarberShops")
                    .document(Common.currentBooking.getCityBooked())
                    .collection("Branch")
                    .document(Common.currentBooking.getCityBooked())
                    .collection("Barber")
                    .document(Common.currentBooking.getBarberId())
                    .collection(Common.convertTimeStampToStringKey(Common.currentBooking.getTimestamp()))
                    .document(Common.currentBooking.getSlot().toString());

            //when we have the document just delete it
            barberbooking.delete()
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    //After Delete from Barber is done
                    //we will start deleting from User
                    deleteBookingFromUser(isChange);
                }
            });

        }
        else
        {
            Toast.makeText(getContext(), "Current Booking must be null", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteBookingFromUser(final boolean isChange) {
        //First get Info from User Object
        if (!TextUtils.isEmpty(Common.currentBookingId))
        {
            DocumentReference userBooking = FirebaseFirestore.getInstance()
                    .collection("User")
                    .document(Common.currentUser.getPhoneNumber())
                    .collection("Booking")
                    .document(Common.currentBookingId);

            //Delete
            userBooking.delete()
                    .addOnFailureListener(e -> Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show())
                    .addOnSuccessListener(aVoid -> {
                        //After delete from "User" , just delete from Calendar
                        //First we need get the saved Uri of the event we just added
                        Paper.init(getActivity());
                        //prevent crash since calendar uri is empty
                        String eventString  = Paper.book().read(Common.EVENT_URI_CACHE).toString();
                        Uri eventUri = null;
                        if (eventString != null && !TextUtils.isEmpty(eventString))
                            eventUri = Uri.parse(eventString);
                        if (eventUri != null)
                            getActivity().getContentResolver().delete(eventUri,null,null);

                        Toast.makeText(getContext(), "Booking Deleted Successfully!! ", Toast.LENGTH_SHORT).show();



                        //Refresh
                        loadUserBooking();

                        //Check if isChange -> called from change Button we'll fire up changeInfo interface
                        if (isChange)
                            ichangeListener.onBookingInfoChanged();

                        dialog.dismiss();
                    });
        }
        else
        {
            dialog.dismiss();

            Toast.makeText(getContext(), "Booking Information Must Not Be Empty", Toast.LENGTH_SHORT).show();
        }
    }

    //FireStore
    CollectionReference bannerRef,lookbookRef;

    //Interface
    IBannerLoadListener iBannerLoadListener;
    ILookBookListener iLookBookListener;
    IBookingInfoLoadListener iBookingInfoLoadListener;
    IBookingInfoChangeListener ichangeListener;

    ListenerRegistration userBookingListener = null;
    EventListener<QuerySnapshot> userBookingEvent = null;

    public HomeFragment() {
        bannerRef = FirebaseFirestore.getInstance().collection("Banner");
        lookbookRef = FirebaseFirestore.getInstance().collection("LookBook");

    }


    @Override
    public void onResume() {
        super.onResume();
        loadUserBooking();
        countCartItems();
    }

    private void loadUserBooking() {
        CollectionReference userBooking = FirebaseFirestore.getInstance()
                .collection("User")
                .document(Common.currentUser.getPhoneNumber())
                .collection("Booking");

        //Get Current Date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,0);
        calendar.add(Calendar.HOUR_OF_DAY,0);
        calendar.add(Calendar.MINUTE,0);

        Timestamp todayStimestamp = new Timestamp(calendar.getTime());

        //Select Booking info from Firebase where "done" == false and timestamp  is greater than today
        userBooking
                .whereGreaterThanOrEqualTo("timestamp",todayStimestamp)
                .whereEqualTo("done",false)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful())
                    {
                        if (!task.getResult().isEmpty())
                        {
                            for (QueryDocumentSnapshot snapshot: task.getResult())
                            {
                                BookingInfo bookingInfo = snapshot.toObject(BookingInfo.class);
                                iBookingInfoLoadListener.onBookingInfoLoadSuccess(bookingInfo,snapshot.getId());
                                break; //exit loop as soon as info is gotten
                            }
                        }
                        else
                            iBookingInfoLoadListener.onBookingInfoLoadEmpty();
                    }
                })
                .addOnFailureListener(e -> iBookingInfoLoadListener.onBookingInfoLoadFailed(e.getMessage()));

        //Here, after userBooking has been assigned data (collections)
        //initiate the real time listener here
        if (userBookingEvent != null) // if the booking event has already been initiated
        {
           if (userBookingListener == null) //only add if the userBookingListener == null
           {
               //jus add once
               userBookingListener = userBooking
                       .addSnapshotListener(userBookingEvent);
           }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new SpotsDialog.Builder().setContext(getContext()).setCancelable(false).build();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this,view);

        database = CartDatabase.getInstance(getContext());

        //Init
        Slider.init(new PicassoImageLoadingService());
        iBannerLoadListener = this;
        iLookBookListener = this;
        iBookingInfoLoadListener = this;
        ichangeListener = this;

        //Check is Logged
        if (AccountKit.getCurrentAccessToken() != null)
        {
            setUserInformation();
            loadBanners();
            LoadLookBook();
            initRealTimeUserBooking(); //declare above user Booking
            loadUserBooking();
            countCartItems();
        }


        return view;
    }

    private void initRealTimeUserBooking() {

        //caution this code could cause an infinite loop in the app and cause you
        // to reach the QUOTA LIMIT from firestore

        if(userBookingEvent == null) //only init event if it is null
        {
            userBookingEvent = new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    //in this event, when it is fired , we will load the user bookings again
                    //to reload all booking info
                    loadUserBooking();

                }
            };
        }

    }

    private void countCartItems() {
        DatabaseUtils.countCartItems(database,this);
    }

    private void LoadLookBook() {
        lookbookRef.get()
                .addOnCompleteListener(task -> {
                    List<Banner> lookbook = new ArrayList<>();

                    if (task.isSuccessful())
                    {
                        for (QueryDocumentSnapshot lookSnap :task.getResult())
                        {
                            Banner banner = lookSnap.toObject(Banner.class);
                            lookbook.add(banner);
                        }

                        iLookBookListener.onLookBookLoadSuccess(lookbook);
                    }
                }).addOnFailureListener(e -> iLookBookListener.onLookBookLoadFailed(e.getMessage()));
    }

    private void loadBanners() {
        bannerRef.get()
                .addOnCompleteListener(task -> {
                    List<Banner> banners = new ArrayList<>();

                    if (task.isSuccessful())
                    {
                        for (QueryDocumentSnapshot bannerSnap :task.getResult())
                        {
                            Banner banner = bannerSnap.toObject(Banner.class);
                            banners.add(banner);
                        }

                        iBannerLoadListener.onBannerLoadSuccess(banners);
                    }
                }).addOnFailureListener(e -> iBannerLoadListener.onBannerLoadFailed(e.getMessage()));
    }

    private void setUserInformation() {
        user_inf_layout.setVisibility(View.VISIBLE);
        txt_user_name.setText(Common.currentUser.getName());
    }

    @Override
    public void onLookBookLoadSuccess(List<Banner> banners) {
        look_book_recycler.setHasFixedSize(true);
        look_book_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        look_book_recycler.setAdapter(new LookBookAdapter(getActivity(),banners));
    }

    @Override
    public void onLookBookLoadFailed(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBannerLoadSuccess(List<Banner> banners) {
        banner_slider.setAdapter(new HomeSliderAdapter(banners));
    }

    @Override
    public void onBannerLoadFailed(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBookingInfoLoadEmpty() {
        booking_card.setVisibility(View.GONE);
    }

    @Override
    public void onBookingInfoLoadSuccess(BookingInfo bookingInfo, String bookingId) {
        Common.currentBooking = bookingInfo;
        //Id needed to delete specific booking and still preserve user booking history
        Common.currentBookingId = bookingId;

        shop_address.setText(bookingInfo.getShopAddress());
        barber_selected.setText(bookingInfo.getBarberName());
        time_selected.setText(bookingInfo.getTime());
        String dateRem = DateUtils.getRelativeTimeSpanString(
                Long.valueOf(bookingInfo.getTimestamp().toDate().getTime()),
                Calendar.getInstance().getTimeInMillis(),0).toString();
        remaining_time.setText(dateRem);

        booking_card.setVisibility(View.VISIBLE);

        dialog.dismiss();
    }

    @Override
    public void onBookingInfoLoadFailed(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBookingInfoChanged() {
        //Just start Activity Booking
        startActivity(new Intent(getActivity(),BookingActivity.class));
    }

    @Override
    public void onCartItemCountSuccess(int count) {
        notificationBadge.setText(String.valueOf(count));
    }

    @Override
    public void onDestroy() {
        if (userBookingListener != null)
            userBookingListener.remove();
        super.onDestroy();
    }
}
