package com.codekid.barberapp.Fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Models.BookingInfo;
import com.codekid.barberapp.Models.FCMResponse;
import com.codekid.barberapp.Models.FCMSendData;
import com.codekid.barberapp.Models.MyToken;
import com.codekid.barberapp.Models.Notification;
import com.codekid.barberapp.R;
import com.codekid.barberapp.Retrofit.IFCMApi;
import com.codekid.barberapp.Retrofit.RetrofitClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class BookingStep4Fragment extends Fragment {


    CompositeDisposable compositeDisposable = new CompositeDisposable();
    SimpleDateFormat simpleDateFormat;
    LocalBroadcastManager localBroadcastManager;

    Unbinder unbinder;

    IFCMApi ifcmApi;

    AlertDialog dialog;

    @BindView(R.id.txt_booked_barber)
    TextView txt_booked_barber;
    @BindView(R.id.txt_booking_time)
    TextView booked_time;
    @BindView(R.id.txt_shop_address)
    TextView shop_address;
    @BindView(R.id.txt_shop_name)
    TextView shop_name;
    @BindView(R.id.txt_salon_website)
    TextView shop_website;
    @BindView(R.id.txt_shop_open_hours)
    TextView shop_open_hours;
    @BindView(R.id.txt_shop_contact)
    TextView shop_contact;
    @OnClick(R.id.btn_confirm)
    void  confirmBooking()
    {
        dialog.show();

        //Process TimeStamp
        //We will use the TimeStamp to filter all bookings later than today
        //Only display future bookings
        String startTime = Common.convertTimeSlotToString(Common.currentTimeSlot);
        String[] convertTime = startTime.split("-"); //Split ex: 9:00 -10:00
        //Get Start Time: get 9:00
        String[] startTimeConversion = convertTime[0].split(":");
        int startHourInt = Integer.parseInt(startTimeConversion[0].trim()); //we get 9
        int startMinInt = Integer.parseInt(startTimeConversion[1].trim()); //we get 00

        Calendar bookingDateWithourHouse = Calendar.getInstance();
        bookingDateWithourHouse.setTimeInMillis(Common.bookingDate.getTimeInMillis());
        bookingDateWithourHouse.set(Calendar.HOUR_OF_DAY,startHourInt);
        bookingDateWithourHouse.set(Calendar.MINUTE,startMinInt);


        //Create TimeStamp object and apply to Booking Info
        Timestamp timestamp = new Timestamp(bookingDateWithourHouse.getTime());

        //Create booking Information
        final BookingInfo bookingInfo = new BookingInfo();

        bookingInfo.setCityBooked(Common.city);
        bookingInfo.setTimestamp(timestamp);
        bookingInfo.setDone(false); //Always FALSE,because we will use this field to filter for display on user
        bookingInfo.setBarberId(Common.currentBarber.getBarberId());
        bookingInfo.setBarberName(Common.currentBarber.getName());
        bookingInfo.setCustomerName(Common.currentUser.getName());
        bookingInfo.setCustomerPhone(Common.currentUser.getPhoneNumber());
        bookingInfo.setShopId(Common.currentShop.getShopId());
        bookingInfo.setShopAddress(Common.currentShop.getAddress());
        bookingInfo.setShopName(Common.currentShop.getName());
        bookingInfo.setTime(new StringBuilder(Common.convertTimeSlotToString(Common.currentTimeSlot))
                .append("at")
                .append(simpleDateFormat.format(bookingDateWithourHouse.getTime())).toString());
        bookingInfo.setSlot(Long.valueOf(Common.currentTimeSlot));

        //submit to Barber
        DocumentReference bookingDate  = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city)
                .collection("Branch")
                .document(Common.currentShop.getShopId())
                .collection("Barbers")
                .document(Common.currentBarber.getBarberId())
                .collection(Common.simpleDateFormat.format(Common.bookingDate.getTime()))
                .document(String.valueOf(Common.currentTimeSlot));

        //Write Data
        bookingDate.set(bookingInfo)
                .addOnSuccessListener(aVoid -> {
                    //Here we can write function to check if a booking has been done
                    //we can prevent new booking

                    addToUserBooking(bookingInfo);

                })
                .addOnFailureListener(e -> Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show());
    }

    private void addToUserBooking(final BookingInfo bookingInfo)
    {

        //First Create new Collection
        final CollectionReference userBooking = FirebaseFirestore.getInstance()
                .collection("Users")
                .document(Common.currentUser.getPhoneNumber())
                .collection("Booking");

        //Check if document already exists in the collection
        //Get Current Date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,0);
        calendar.add(Calendar.HOUR_OF_DAY,0);
        calendar.add(Calendar.MINUTE,0);

        Timestamp todayStimestamp = new Timestamp(calendar.getTime());


        userBooking.whereGreaterThanOrEqualTo("timestamp",todayStimestamp)
                .whereEqualTo("done",false)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.getResult().isEmpty())
                    {
                        //Set Data
                        userBooking.document()
                                .set(bookingInfo)
                                .addOnSuccessListener(aVoid -> {

                                    //Create Notification
                                    Notification notification = new Notification();
                                    notification.setUid(UUID.randomUUID().toString());
                                    notification.setTitle("New Booking");
                                    notification.setContent("You have an appointment for customer Hair Care with  "+Common.currentUser.getName());
                                    notification.setRead(false); //filter
                                    notification.setServerTimeStamp(FieldValue.serverTimestamp()); //this code gets the timestamp of the server

                                    //Submit notification to Barbers Notification collection
                                    FirebaseFirestore.getInstance()
                                            .collection("BarberShops")
                                            .document(Common.city)
                                            .collection("Branch")
                                            .document(Common.currentShop.getShopId())
                                            .collection("Barber")
                                            .document(Common.currentBarber.getBarberId())
                                            .collection("Notifications")
                                            .document(notification.getUid()) //create unique key
                                            .set(notification)
                                            .addOnSuccessListener(aVoid1 -> {

                                                //get token based on barberId
                                                FirebaseFirestore.getInstance()
                                                        .collection("Tokens")
                                                        .whereEqualTo("userPhone",Common.currentBarber.getUsername())
                                                        .limit(1)
                                                        .get()
                                                        .addOnCompleteListener(task1 -> {
                                                            if (task1.isSuccessful() && task1.getResult().size() > 0 ){
                                                                MyToken myToken = new MyToken();
                                                                for (DocumentSnapshot snapshot : task1.getResult())
                                                                    myToken = snapshot.toObject(MyToken.class);

                                                                //create data to send
                                                                FCMSendData request = new FCMSendData();
                                                                Map<String,String> dataSend = new HashMap<>();
                                                                dataSend.put(Common.TITLE_KEY,"New Booking");
                                                                dataSend.put(Common.CONTENT_KEY,"You have a new client booking with "+ Common.currentUser.getName());

                                                                request.setTo(myToken.getToken());
                                                                request.setData(dataSend);

                                                                compositeDisposable.add(
                                                                         ifcmApi.sendNotification(request)
                                                                                .subscribeOn(Schedulers.io())
                                                                                .observeOn(AndroidSchedulers.mainThread())
                                                                                .subscribe(fcmResponse -> {
                                                                                    dialog.dismiss();

                                                                                    addToCalendar(Common.bookingDate,Common.convertTimeSlotToString(Common.currentTimeSlot));
                                                                                    resetStaticData();
                                                                                    getActivity().finish();
                                                                                    Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();

                                                                                }, throwable ->{
                                                                                    Log.d("NOTIFICATION ERROR",throwable.getMessage());


                                                                                    addToCalendar(Common.bookingDate,Common.convertTimeSlotToString(Common.currentTimeSlot));
                                                                                    resetStaticData();
                                                                                    getActivity().finish();
                                                                                    Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();



                                                                                } )
                                                                );
                                                            }
                                                        });
                                            });

                                })
                                .addOnFailureListener(e -> {
                                    if (dialog.isShowing())
                                        dialog.dismiss();
                                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                });
                    }
                    else
                    {
                        if (dialog.isShowing())
                            dialog.dismiss();

                        resetStaticData();
                        getActivity().finish(); //close Activity
                        Toast.makeText(getContext(), "Success!", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void addToCalendar(Calendar bookingDate, String startDate)
    {
        String startTime = Common.convertTimeSlotToString(Common.currentTimeSlot);
        String[] convertTime = startTime.split("-"); //Split ex: 9:00 -10:00
        //Get Start Time: get 9:00
        String[] startTimeConversion = convertTime[0].split(":");
        int startHourInt = Integer.parseInt(startTimeConversion[0].trim()); //we get 9
        int startMinInt = Integer.parseInt(startTimeConversion[1].trim()); //we get 00

        String[] endTimeConversion = convertTime[1].split(":");
        int endHourInt = Integer.parseInt(endTimeConversion[0].trim()); //we get 10
        int endMinInt = Integer.parseInt(endTimeConversion[1].trim()); //we get 00

        Calendar startEvent = Calendar.getInstance();
        startEvent.setTimeInMillis(bookingDate.getTimeInMillis());
        startEvent.set(Calendar.HOUR_OF_DAY,startHourInt); //start Event Start Hour
        startEvent.set(Calendar.MINUTE,startMinInt); //set event start min

        Calendar endEvent = Calendar.getInstance();
        endEvent.setTimeInMillis(bookingDate.getTimeInMillis());
        endEvent.set(Calendar.HOUR_OF_DAY,endHourInt); //set end time (hr) of event
        endEvent.set(Calendar.MINUTE,endMinInt);

        //After setting startTime and EndTime of Event convert it to String Format
        SimpleDateFormat calendarDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String startEventTime = calendarDateFormat.format(startEvent.getTime());
        String endEventTime = calendarDateFormat.format(endEvent.getTime());

        addToDeviceCalendar(startEventTime,endEventTime,"Haircut Booking",
                new StringBuilder("Haircut from")
                        .append(startTime)
                        .append(" with ")
                        .append(Common.currentBarber.getName())
                        .append(" at ")
                        .append(Common.currentShop.getName()).toString(),
                new StringBuilder(" Address ").append(Common.currentShop.getAddress()).toString());
    }

    private void addToDeviceCalendar(String startEventTime, String endEventTime, String title, String description, String location) {
        SimpleDateFormat calendarDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        try {
            Date start = calendarDateFormat.parse(startEventTime);
            Date end = calendarDateFormat.parse(endEventTime);

            ContentValues event = new ContentValues();

            //Put
            event.put(CalendarContract.Events.CALENDAR_ID,getCalendar(getContext()));
            event.put(CalendarContract.Events.TITLE,title);
            event.put(CalendarContract.Events.DESCRIPTION,description);
            event.put(CalendarContract.Events.EVENT_LOCATION,location);

            //Time
            event.put(CalendarContract.Events.DTSTART,start.getTime());
            event.put(CalendarContract.Events.DTEND,end.getTime());
            event.put(CalendarContract.Events.ALL_DAY,0);
            event.put(CalendarContract.Events.HAS_ALARM,1);

            String timeZone = TimeZone.getDefault().getID();
            event.put(CalendarContract.Events.EVENT_TIMEZONE,timeZone);

            Uri calendars ;

            if (Build.VERSION.SDK_INT >= 8)
                calendars =  Uri.parse("content://com.android.calendar/events");
            else
                calendars = Uri.parse("content://calendar/events");

            Uri uri_save = getActivity().getContentResolver().insert(calendars,event);
            //Save to cache
            Paper.init(getActivity());
            Paper.book().write(Common.EVENT_URI_CACHE,uri_save.toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private String getCalendar(Context context)
    {
        //Get default calendar ID of Gmail Calendar
        String gmailIdCalendar = "";

        String projection[]={"_id","calendar_displayName"};
        Uri calendars = Uri.parse("content://com.android.calendar/calendars");

        ContentResolver contentResolver = context.getContentResolver();
        //Select All Calendars
        Cursor managedCursor = contentResolver.query(calendars,projection,null,null,null);

        if (managedCursor.moveToFirst())
        {
            String calName;
            int nameCol = managedCursor.getColumnIndex(projection[1]);
            int idCol = managedCursor.getColumnIndex(projection[0]);
            do {
                calName = managedCursor.getString(nameCol);
                if (calName.contains("@gmail.com"))
                {
                    gmailIdCalendar = managedCursor.getString(idCol);
                    break; //exit as soon as we acquire id
                }
            }while (managedCursor.moveToNext());
            managedCursor.close();
        }

        return gmailIdCalendar;
    }

    private void resetStaticData(){
        Common.step = 0;
        Common.currentTimeSlot = -1;
        Common.currentShop = null;
        Common.currentBarber = null;
        Common.bookingDate.add(Calendar.DATE,0); //Current Date Added
    }

    BroadcastReceiver confirmBookingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setData();
        }
    };

    private void setData() {
        txt_booked_barber.setText(Common.currentBarber.getName());
        booked_time.setText(new StringBuilder(Common.convertTimeSlotToString(Common.currentTimeSlot))
                .append(" at ")
                .append(simpleDateFormat.format(Common.bookingDate.getTime())));

        shop_address.setText(Common.currentShop.getAddress());
        shop_name.setText(Common.currentShop.getName());
        shop_website.setText(Common.currentShop.getWebsite());
        shop_open_hours.setText(Common.currentShop.getOpenHours());
    }

    static BookingStep4Fragment instance;

    public static BookingStep4Fragment getInstance(){
        if (instance == null)
            instance = new BookingStep4Fragment();
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ifcmApi = RetrofitClient.getRetrofit().create(IFCMApi.class);

        dialog = new SpotsDialog.Builder().setContext(getContext()).setCancelable(false).build();

        //Date display
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());

        localBroadcastManager.registerReceiver(confirmBookingReceiver,new IntentFilter(Common.KEY_CONFIRM_BOOKING));
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(confirmBookingReceiver);
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View itemView = inflater.inflate(R.layout.fragment_booking_step_four,container,false);
        unbinder = ButterKnife.bind(this,itemView);

        return itemView;
    }
}
