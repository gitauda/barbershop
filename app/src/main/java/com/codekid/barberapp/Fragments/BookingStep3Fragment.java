package com.codekid.barberapp.Fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codekid.barberapp.Adapter.TimeSlotAdapter;
import com.codekid.barberapp.Common.Common;
import com.codekid.barberapp.Common.SpaceItemDecoration;
import com.codekid.barberapp.Interface.ITimeSlotListener;
import com.codekid.barberapp.Models.TimeSlot;
import com.codekid.barberapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import dmax.dialog.SpotsDialog;

public class BookingStep3Fragment extends Fragment implements ITimeSlotListener {

    //Variables
    DocumentReference barberDoc;
    ITimeSlotListener timeSlotListener;
    AlertDialog dialog;

    Unbinder unbinder;
    LocalBroadcastManager localBroadcastManager;


    @BindView(R.id.time_recycler)
    RecyclerView time_recycler;
    @BindView(R.id.calendar_view)
    HorizontalCalendarView calendarView;
    SimpleDateFormat dateFormat;

    BroadcastReceiver displayTimeSlot = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Calendar date = Calendar.getInstance();
            date.add(Calendar.DATE,0); // Add Current Date
            loadAllAvailableTimeSlots(Common.currentBarber.getBarberId(),
                    dateFormat.format(date.getTime()));
        }
    };

    private void loadAllAvailableTimeSlots(String barberId,final String bookDate) {
        dialog.show();
        ///BarberShops/Nairobi/Branch/brv6UA3HkpP1H309IwGS/Barbers/eR5ZHuVOstBBDt72ZLtB

        barberDoc = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city)
                .collection("Branch")
                .document(Common.currentShop.getShopId())
                .collection("Barbers")
                .document(barberId);

        //Get Barber's information
        barberDoc.get()
                .addOnCompleteListener(task -> {
                    final DocumentSnapshot snapshot = task.getResult();
                    if (snapshot.exists()) // if Barber is available
                    {
                        //Get Booking info
                        //if not created return empty
                        CollectionReference date = FirebaseFirestore.getInstance()
                                .collection("BarberShops")
                                .document(Common.city)
                                .collection("Branch")
                                .document(Common.currentShop.getShopId())
                                .collection("Barbers")
                                .document(Common.currentBarber.getBarberId())
                                .collection(bookDate);

                        date.get()
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful())
                                    {
                                        QuerySnapshot snapshots = task1.getResult();
                                        if (snapshots.isEmpty()) // if there is no appointment
                                            timeSlotListener.onTimeSlotLoadEmpty();
                                        else
                                        {
                                            //if there is an appointment
                                            List<TimeSlot> timeSlots = new ArrayList<>();
                                            for (QueryDocumentSnapshot docSnap: task1.getResult())
                                                timeSlots.add(docSnap.toObject(TimeSlot.class));
                                            timeSlotListener.onTimeSlotLoadSuccess(timeSlots);
                                        }
                                    }
                                }).addOnFailureListener(e -> timeSlotListener.onTimeSlotLoadFailure(e.getMessage()));
                    }
                });
    }

    static BookingStep3Fragment instance;

    public static BookingStep3Fragment getInstance(){
        if (instance == null)
            instance = new BookingStep3Fragment();
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        timeSlotListener = this;

        localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        localBroadcastManager.registerReceiver(displayTimeSlot,new IntentFilter(Common.KEY_DISPLAY_TIME_SLOT));

        dateFormat = new SimpleDateFormat("dd_MM_yyyy"); //03_05_2019 (this is key)

        dialog = new SpotsDialog.Builder().setContext(getContext()).setCancelable(false).build();

      }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(displayTimeSlot);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View itemView = inflater.inflate(R.layout.fragment_booking_step_three,container,false);
        unbinder = ButterKnife.bind(this,itemView);

        init(itemView);

        return  itemView;
    }

    private void init(View itemView) {
        time_recycler.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        time_recycler.setLayoutManager(gridLayoutManager);
        time_recycler.addItemDecoration(new SpaceItemDecoration(8));

        //Calendar
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DATE,0);
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE,2); //2 days

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(itemView,R.id.calendar_view)
                .range(startDate,endDate)
                .datesNumberOnScreen(1)
                .mode(HorizontalCalendar.Mode.DAYS)
                .defaultSelectedDate(startDate)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                if (Common.bookingDate.getTimeInMillis() != date.getTimeInMillis())
                {
                    Common.bookingDate = date; // This code will not load again if u select new Date with same date selected
                    loadAllAvailableTimeSlots(Common.currentBarber.getBarberId(),
                            dateFormat.format(date.getTime()));
                }
            }
        });
    }

    @Override
    public void onTimeSlotLoadSuccess(List<TimeSlot> timeSlots) {
        TimeSlotAdapter adapter = new TimeSlotAdapter(getContext(),timeSlots);
        time_recycler.setAdapter(adapter);

        dialog.dismiss();
    }

    @Override
    public void onTimeSlotLoadFailure(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    @Override
    public void onTimeSlotLoadEmpty() {
        TimeSlotAdapter adapter = new TimeSlotAdapter(getContext());
        time_recycler.setAdapter(adapter);

        dialog.dismiss();
    }
}
